﻿using System.Xml.Serialization;

namespace XMLJean.Objetos
{
    public class ObjBeneficiario
    {
        [XmlRoot(ElementName = "identificacao")]
        public class Identificacao
        {

            [XmlElement(ElementName = "cpf")]
            public string? Cpf { get; set; }

            [XmlElement(ElementName = "cns")]
            public string? Cns { get; set; }

            [XmlElement(ElementName = "nome")]
            public string? Nome { get; set; }

            [XmlElement(ElementName = "sexo")]
            public string? Sexo { get; set; }

            [XmlElement(ElementName = "dataNascimento")]
            public string? DataNascimento { get; set; }

            [XmlElement(ElementName = "nomeMae")]
            public string? NomeMae { get; set; }
        }

        [XmlRoot(ElementName = "endereco")]
        public class Endereco
        {

            [XmlElement(ElementName = "logradouro")]
            public string? Logradouro { get; set; }

            [XmlElement(ElementName = "numero")]
            public string? Numero { get; set; }

            [XmlElement(ElementName = "complemento")]
            public string? Complemento { get; set; }

            [XmlElement(ElementName = "bairro")]
            public string? Bairro { get; set; }

            [XmlElement(ElementName = "codigoMunicipio")]
            public string? CodigoMunicipio { get; set; }

            [XmlElement(ElementName = "cep")]
            public string? Cep { get; set; }

            [XmlElement(ElementName = "tipoEndereco")]
            public string? TipoEndereco { get; set; }

            [XmlElement(ElementName = "resideExterior")]
            public string? ResideExterior { get; set; }
        }

        [XmlRoot(ElementName = "vinculo")]
        public class Vinculo
        {

            [XmlElement(ElementName = "codigoBeneficiario")]
            public string? CodigoBeneficiario { get; set; }

            [XmlElement(ElementName = "relacaoDependencia")]
            public string? RelacaoDependencia { get; set; }

            [XmlElement(ElementName = "ccoBeneficiarioTitular")]
            public string? CcoBeneficiarioTitular { get; set; }

            [XmlElement(ElementName = "dataPrimeiraContratacao")]
            public string? DataPrimeiraContratacao { get; set; }

            [XmlElement(ElementName = "dataContratacao")]
            public string? DataContratacao { get; set; }

            [XmlElement(ElementName = "dataCancelamento")]
            public string? DataCancelamento { get; set; }

            [XmlElement(ElementName = "motivoCancelamento")]
            public string? MotivoCancelamento { get; set; }

            [XmlElement(ElementName = "numeroPlanoANS")]
            public string? NumeroPlanoANS { get; set; }

            [XmlElement(ElementName = "coberturaParcialTemporaria")]
            public string? CoberturaParcialTemporaria { get; set; }

            [XmlElement(ElementName = "itensExcluidosCobertura")]
            public string? ItensExcluidosCobertura { get; set; }

            [XmlElement(ElementName = "cnpjEmpresaContratante")]
            public string? CnpjEmpresaContratante { get; set; }
        }

        [XmlRoot(ElementName = "beneficiario")]
        public class Beneficiario
        {

            [XmlElement(ElementName = "identificacao")]
            public Identificacao? Identificacao { get; set; }

            [XmlElement(ElementName = "endereco")]
            public Endereco? Endereco { get; set; }

            [XmlElement(ElementName = "vinculo")]
            public Vinculo? Vinculo { get; set; }

            [XmlAttribute(AttributeName = "cco")]
            public string? Cco { get; set; }

            [XmlAttribute(AttributeName = "situacao")]
            public string? Situacao { get; set; }

            [XmlAttribute(AttributeName = "dataAtualizacao")]
            public string? DataAtualizacao { get; set; }

            [XmlText]
            public string? Text { get; set; }
        }


    }
}
