﻿/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
public  class mensagemSIB {
    
    private ct_cabecalho_transacao cabecalhoField;
    
    private mensagemSIBMensagem mensagemField;
    
    private ct_epilogo epilogoField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ct_cabecalho_transacao cabecalho {
        get {
            return this.cabecalhoField;
        }
        set {
            this.cabecalhoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public mensagemSIBMensagem mensagem {
        get {
            return this.mensagemField;
        }
        set {
            this.mensagemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ct_epilogo epilogo {
        get {
            return this.epilogoField;
        }
        set {
            this.epilogoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_cabecalho_transacao {
    
    private ct_cabecalho_transacaoIdentificacaoTransacao identificacaoTransacaoField;
    
    private ct_falha_negocioFalha[] falhaNegocioField;
    
    private ct_cabecalho_transacaoOrigem origemField;
    
    private ct_cabecalho_transacaoDestino destinoField;
    
    private st_nr_versao versaoPadraoField;
    
    private ct_identificacao_software_gerador identificacaoSoftwareGeradorField;
    
    /// <remarks/>
    public ct_cabecalho_transacaoIdentificacaoTransacao identificacaoTransacao {
        get {
            return this.identificacaoTransacaoField;
        }
        set {
            this.identificacaoTransacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlArrayItemAttribute("falha", IsNullable=false)]
    public ct_falha_negocioFalha[] falhaNegocio {
        get {
            return this.falhaNegocioField;
        }
        set {
            this.falhaNegocioField = value;
        }
    }
    
    /// <remarks/>
    public ct_cabecalho_transacaoOrigem origem {
        get {
            return this.origemField;
        }
        set {
            this.origemField = value;
        }
    }
    
    /// <remarks/>
    public ct_cabecalho_transacaoDestino destino {
        get {
            return this.destinoField;
        }
        set {
            this.destinoField = value;
        }
    }
    
    /// <remarks/>
    public st_nr_versao versaoPadrao {
        get {
            return this.versaoPadraoField;
        }
        set {
            this.versaoPadraoField = value;
        }
    }
    
    /// <remarks/>
    public ct_identificacao_software_gerador identificacaoSoftwareGerador {
        get {
            return this.identificacaoSoftwareGeradorField;
        }
        set {
            this.identificacaoSoftwareGeradorField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_cabecalho_transacaoIdentificacaoTransacao {
    
    private string tipoTransacaoField;
    
    private string sequencialTransacaoField;
    
    private System.DateTime dataHoraRegistroTransacaoField;
    
    /// <remarks/>
    public string tipoTransacao {
        get {
            return this.tipoTransacaoField;
        }
        set {
            this.tipoTransacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string sequencialTransacao {
        get {
            return this.sequencialTransacaoField;
        }
        set {
            this.sequencialTransacaoField = value;
        }
    }
    
    /// <remarks/>
    public System.DateTime dataHoraRegistroTransacao {
        get {
            return this.dataHoraRegistroTransacaoField;
        }
        set {
            this.dataHoraRegistroTransacaoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_epilogo {
    
    private string hashField;
    
    /// <remarks/>
    public string hash {
        get {
            return this.hashField;
        }
        set {
            this.hashField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_conferencia_transferencia {
    
    private ct_conferencia_transferenciaBeneficiario[] beneficiarioField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("beneficiario")]
    public ct_conferencia_transferenciaBeneficiario[] beneficiario {
        get {
            return this.beneficiarioField;
        }
        set {
            this.beneficiarioField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferencia_transferenciaBeneficiario {
    
    private ct_conferencia_transferenciaBeneficiarioIdentificacao identificacaoField;
    
    private ct_conferencia_transferenciaBeneficiarioEndereco enderecoField;
    
    private ct_conferencia_transferenciaBeneficiarioVinculo vinculoField;
    
    private string ccoField;
    
    private string situacaoField;
    
    private string dataAtualizacaoField;
    
    private string situacaoTransferenciaField;
    
    /// <remarks/>
    public ct_conferencia_transferenciaBeneficiarioIdentificacao identificacao {
        get {
            return this.identificacaoField;
        }
        set {
            this.identificacaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_conferencia_transferenciaBeneficiarioEndereco endereco {
        get {
            return this.enderecoField;
        }
        set {
            this.enderecoField = value;
        }
    }
    
    /// <remarks/>
    public ct_conferencia_transferenciaBeneficiarioVinculo vinculo {
        get {
            return this.vinculoField;
        }
        set {
            this.vinculoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string situacao {
        get {
            return this.situacaoField;
        }
        set {
            this.situacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string dataAtualizacao {
        get {
            return this.dataAtualizacaoField;
        }
        set {
            this.dataAtualizacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string situacaoTransferencia {
        get {
            return this.situacaoTransferenciaField;
        }
        set {
            this.situacaoTransferenciaField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferencia_transferenciaBeneficiarioIdentificacao : ct_identificacao_beneficiario_conferencia {
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_identificacao_beneficiario_conferencia {
    
    private string cpfField;
    
    private string dnField;
    
    private string pisPasepField;
    
    private string cnsField;
    
    private string nomeField;
    
    private st_cd_sexo sexoField;
    
    private System.DateTime dataNascimentoField;
    
    private string nomeMaeField;
    
    /// <remarks/>
    public string cpf {
        get {
            return this.cpfField;
        }
        set {
            this.cpfField = value;
        }
    }
    
    /// <remarks/>
    public string dn {
        get {
            return this.dnField;
        }
        set {
            this.dnField = value;
        }
    }
    
    /// <remarks/>
    public string pisPasep {
        get {
            return this.pisPasepField;
        }
        set {
            this.pisPasepField = value;
        }
    }
    
    /// <remarks/>
    public string cns {
        get {
            return this.cnsField;
        }
        set {
            this.cnsField = value;
        }
    }
    
    /// <remarks/>
    public string nome {
        get {
            return this.nomeField;
        }
        set {
            this.nomeField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_sexo sexo {
        get {
            return this.sexoField;
        }
        set {
            this.sexoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataNascimento {
        get {
            return this.dataNascimentoField;
        }
        set {
            this.dataNascimentoField = value;
        }
    }
    
    /// <remarks/>
    public string nomeMae {
        get {
            return this.nomeMaeField;
        }
        set {
            this.nomeMaeField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_cd_sexo {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1")]
    Item1,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("3")]
    Item3,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferencia_transferenciaBeneficiarioEndereco : ct_endereco_beneficiario_conferencia {
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_endereco_beneficiario_conferencia {
    
    private string logradouroField;
    
    private string numeroField;
    
    private string complementoField;
    
    private string bairroField;
    
    private string codigoMunicipioField;
    
    private string codigoMunicipioResidenciaField;
    
    private string cepField;
    
    private st_tp_endereco tipoEnderecoField;
    
    private bool tipoEnderecoFieldSpecified;
    
    private st_in_reside_exterior resideExteriorField;
    
    private bool resideExteriorFieldSpecified;
    
    /// <remarks/>
    public string logradouro {
        get {
            return this.logradouroField;
        }
        set {
            this.logradouroField = value;
        }
    }
    
    /// <remarks/>
    public string numero {
        get {
            return this.numeroField;
        }
        set {
            this.numeroField = value;
        }
    }
    
    /// <remarks/>
    public string complemento {
        get {
            return this.complementoField;
        }
        set {
            this.complementoField = value;
        }
    }
    
    /// <remarks/>
    public string bairro {
        get {
            return this.bairroField;
        }
        set {
            this.bairroField = value;
        }
    }
    
    /// <remarks/>
    public string codigoMunicipio {
        get {
            return this.codigoMunicipioField;
        }
        set {
            this.codigoMunicipioField = value;
        }
    }
    
    /// <remarks/>
    public string codigoMunicipioResidencia {
        get {
            return this.codigoMunicipioResidenciaField;
        }
        set {
            this.codigoMunicipioResidenciaField = value;
        }
    }
    
    /// <remarks/>
    public string cep {
        get {
            return this.cepField;
        }
        set {
            this.cepField = value;
        }
    }
    
    /// <remarks/>
    public st_tp_endereco tipoEndereco {
        get {
            return this.tipoEnderecoField;
        }
        set {
            this.tipoEnderecoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool tipoEnderecoSpecified {
        get {
            return this.tipoEnderecoFieldSpecified;
        }
        set {
            this.tipoEnderecoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_in_reside_exterior resideExterior {
        get {
            return this.resideExteriorField;
        }
        set {
            this.resideExteriorField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool resideExteriorSpecified {
        get {
            return this.resideExteriorFieldSpecified;
        }
        set {
            this.resideExteriorFieldSpecified = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_tp_endereco {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1")]
    Item1,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("2")]
    Item2,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_in_reside_exterior {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("0")]
    Item0,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1")]
    Item1,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferencia_transferenciaBeneficiarioVinculo : ct_vinculo_conferencia {
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_vinculo_conferencia {
    
    private string codigoBeneficiarioField;
    
    private st_cd_tipo_relacao_dependencia relacaoDependenciaField;
    
    private bool relacaoDependenciaFieldSpecified;
    
    private string ccoBeneficiarioTitularField;
    
    private System.DateTime dataPrimeiraContratacaoField;
    
    private bool dataPrimeiraContratacaoFieldSpecified;
    
    private System.DateTime dataContratacaoField;
    
    private System.DateTime dataReativacaoField;
    
    private bool dataReativacaoFieldSpecified;
    
    private System.DateTime dataCancelamentoField;
    
    private bool dataCancelamentoFieldSpecified;
    
    private st_cd_motivo_cancelamento motivoCancelamentoField;
    
    private bool motivoCancelamentoFieldSpecified;
    
    private string[] itemsField;
    
    private ItemsChoiceType3[] itemsElementNameField;
    
    private st_in_cobertura_parcial_temporaria coberturaParcialTemporariaField;
    
    private bool coberturaParcialTemporariaFieldSpecified;
    
    private st_in_itens_excluidos_cobertura itensExcluidosCoberturaField;
    
    private bool itensExcluidosCoberturaFieldSpecified;
    
    private string itemField;
    
    private ItemChoiceType5 itemElementNameField;
    
    /// <remarks/>
    public string codigoBeneficiario {
        get {
            return this.codigoBeneficiarioField;
        }
        set {
            this.codigoBeneficiarioField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_tipo_relacao_dependencia relacaoDependencia {
        get {
            return this.relacaoDependenciaField;
        }
        set {
            this.relacaoDependenciaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool relacaoDependenciaSpecified {
        get {
            return this.relacaoDependenciaFieldSpecified;
        }
        set {
            this.relacaoDependenciaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public string ccoBeneficiarioTitular {
        get {
            return this.ccoBeneficiarioTitularField;
        }
        set {
            this.ccoBeneficiarioTitularField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataPrimeiraContratacao {
        get {
            return this.dataPrimeiraContratacaoField;
        }
        set {
            this.dataPrimeiraContratacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool dataPrimeiraContratacaoSpecified {
        get {
            return this.dataPrimeiraContratacaoFieldSpecified;
        }
        set {
            this.dataPrimeiraContratacaoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataContratacao {
        get {
            return this.dataContratacaoField;
        }
        set {
            this.dataContratacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataReativacao {
        get {
            return this.dataReativacaoField;
        }
        set {
            this.dataReativacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool dataReativacaoSpecified {
        get {
            return this.dataReativacaoFieldSpecified;
        }
        set {
            this.dataReativacaoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataCancelamento {
        get {
            return this.dataCancelamentoField;
        }
        set {
            this.dataCancelamentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool dataCancelamentoSpecified {
        get {
            return this.dataCancelamentoFieldSpecified;
        }
        set {
            this.dataCancelamentoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_cd_motivo_cancelamento motivoCancelamento {
        get {
            return this.motivoCancelamentoField;
        }
        set {
            this.motivoCancelamentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool motivoCancelamentoSpecified {
        get {
            return this.motivoCancelamentoFieldSpecified;
        }
        set {
            this.motivoCancelamentoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoANS", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoOperadora", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoPortabilidade", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
    public string[] Items {
        get {
            return this.itemsField;
        }
        set {
            this.itemsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemsChoiceType3[] ItemsElementName {
        get {
            return this.itemsElementNameField;
        }
        set {
            this.itemsElementNameField = value;
        }
    }
    
    /// <remarks/>
    public st_in_cobertura_parcial_temporaria coberturaParcialTemporaria {
        get {
            return this.coberturaParcialTemporariaField;
        }
        set {
            this.coberturaParcialTemporariaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool coberturaParcialTemporariaSpecified {
        get {
            return this.coberturaParcialTemporariaFieldSpecified;
        }
        set {
            this.coberturaParcialTemporariaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_in_itens_excluidos_cobertura itensExcluidosCobertura {
        get {
            return this.itensExcluidosCoberturaField;
        }
        set {
            this.itensExcluidosCoberturaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool itensExcluidosCoberturaSpecified {
        get {
            return this.itensExcluidosCoberturaFieldSpecified;
        }
        set {
            this.itensExcluidosCoberturaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("caepfEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("ceiEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("cnpjEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
    public string Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemChoiceType5 ItemElementName {
        get {
            return this.itemElementNameField;
        }
        set {
            this.itemElementNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_cd_tipo_relacao_dependencia {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1")]
    Item1,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("3")]
    Item3,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("4")]
    Item4,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("6")]
    Item6,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("8")]
    Item8,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("10")]
    Item10,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_cd_motivo_cancelamento {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("41")]
    Item41,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("42")]
    Item42,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("43")]
    Item43,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("44")]
    Item44,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("46")]
    Item46,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("47")]
    Item47,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("48")]
    Item48,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemsChoiceType3 {
    
    /// <remarks/>
    numeroPlanoANS,
    
    /// <remarks/>
    numeroPlanoOperadora,
    
    /// <remarks/>
    numeroPlanoPortabilidade,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_in_cobertura_parcial_temporaria {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("0")]
    Item0,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1")]
    Item1,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_in_itens_excluidos_cobertura {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("0")]
    Item0,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1")]
    Item1,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemChoiceType5 {
    
    /// <remarks/>
    caepfEmpresaContratante,
    
    /// <remarks/>
    ceiEmpresaContratante,
    
    /// <remarks/>
    cnpjEmpresaContratante,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_tab {
    
    private System.DateTime dataExecucaoProcessoField;
    
    private System.DateTime dataEfetivaTransferenciaField;
    
    private string regsAnsOperadoraCedenteField;
    
    private string regsAnsOperadoraAdquirenteField;
    
    private long idField;
    
    private bool idFieldSpecified;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataExecucaoProcesso {
        get {
            return this.dataExecucaoProcessoField;
        }
        set {
            this.dataExecucaoProcessoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataEfetivaTransferencia {
        get {
            return this.dataEfetivaTransferenciaField;
        }
        set {
            this.dataEfetivaTransferenciaField = value;
        }
    }
    
    /// <remarks/>
    public string regsAnsOperadoraCedente {
        get {
            return this.regsAnsOperadoraCedenteField;
        }
        set {
            this.regsAnsOperadoraCedenteField = value;
        }
    }
    
    /// <remarks/>
    public string regsAnsOperadoraAdquirente {
        get {
            return this.regsAnsOperadoraAdquirenteField;
        }
        set {
            this.regsAnsOperadoraAdquirenteField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public long id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool idSpecified {
        get {
            return this.idFieldSpecified;
        }
        set {
            this.idFieldSpecified = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_conferencia {
    
    private ct_conferenciaBeneficiario[] beneficiarioField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("beneficiario")]
    public ct_conferenciaBeneficiario[] beneficiario {
        get {
            return this.beneficiarioField;
        }
        set {
            this.beneficiarioField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferenciaBeneficiario {
    
    private ct_conferenciaBeneficiarioIdentificacao identificacaoField;
    
    private ct_conferenciaBeneficiarioEndereco enderecoField;
    
    private ct_conferenciaBeneficiarioVinculo vinculoField;
    
    private string situacaoTransfrenciaField;
    
    private string ccoField;
    
    private string situacaoField;
    
    private string dataAtualizacaoField;
    
    /// <remarks/>
    public ct_conferenciaBeneficiarioIdentificacao identificacao {
        get {
            return this.identificacaoField;
        }
        set {
            this.identificacaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_conferenciaBeneficiarioEndereco endereco {
        get {
            return this.enderecoField;
        }
        set {
            this.enderecoField = value;
        }
    }
    
    /// <remarks/>
    public ct_conferenciaBeneficiarioVinculo vinculo {
        get {
            return this.vinculoField;
        }
        set {
            this.vinculoField = value;
        }
    }
    
    /// <remarks/>
    public string situacaoTransfrencia {
        get {
            return this.situacaoTransfrenciaField;
        }
        set {
            this.situacaoTransfrenciaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string situacao {
        get {
            return this.situacaoField;
        }
        set {
            this.situacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string dataAtualizacao {
        get {
            return this.dataAtualizacaoField;
        }
        set {
            this.dataAtualizacaoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferenciaBeneficiarioIdentificacao : ct_identificacao_beneficiario_conferencia {
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferenciaBeneficiarioEndereco : ct_endereco_beneficiario_conferencia {
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_conferenciaBeneficiarioVinculo : ct_vinculo_conferencia {
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_consolidado_processamento {
    
    private string quantidadeRegistrosField;
    
    private string quantidadeProcessadosField;
    
    private string quantidadeRejeitadosField;
    
    private double percentualAcertoField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string quantidadeRegistros {
        get {
            return this.quantidadeRegistrosField;
        }
        set {
            this.quantidadeRegistrosField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string quantidadeProcessados {
        get {
            return this.quantidadeProcessadosField;
        }
        set {
            this.quantidadeProcessadosField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string quantidadeRejeitados {
        get {
            return this.quantidadeRejeitadosField;
        }
        set {
            this.quantidadeRejeitadosField = value;
        }
    }
    
    /// <remarks/>
    public double percentualAcerto {
        get {
            return this.percentualAcertoField;
        }
        set {
            this.percentualAcertoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_erro_consolidado {
    
    private string codigoErroField;
    
    private string mensagemErroField;
    
    private string quantidadeOcorrenciaErroField;
    
    /// <remarks/>
    public string codigoErro {
        get {
            return this.codigoErroField;
        }
        set {
            this.codigoErroField = value;
        }
    }
    
    /// <remarks/>
    public string mensagemErro {
        get {
            return this.mensagemErroField;
        }
        set {
            this.mensagemErroField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string quantidadeOcorrenciaErro {
        get {
            return this.quantidadeOcorrenciaErroField;
        }
        set {
            this.quantidadeOcorrenciaErroField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_cco {
    
    private string ccoField;
    
    private string nomeBeneficiarioField;
    
    /// <remarks/>
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    public string nomeBeneficiario {
        get {
            return this.nomeBeneficiarioField;
        }
        set {
            this.nomeBeneficiarioField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_erro_generico {
    
    private string codigoErroField;
    
    private string mensagemErroField;
    
    /// <remarks/>
    public string codigoErro {
        get {
            return this.codigoErroField;
        }
        set {
            this.codigoErroField = value;
        }
    }
    
    /// <remarks/>
    public string mensagemErro {
        get {
            return this.mensagemErroField;
        }
        set {
            this.mensagemErroField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_erro {
    
    private ct_erroCampoErro[] campoErroField;
    
    private string ccoField;
    
    private string codigoBeneficiarioField;
    
    private st_cd_tipo_movimento codigoTipoMovimentoField;
    
    private string tipoMovimentoField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("campoErro")]
    public ct_erroCampoErro[] campoErro {
        get {
            return this.campoErroField;
        }
        set {
            this.campoErroField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string codigoBeneficiario {
        get {
            return this.codigoBeneficiarioField;
        }
        set {
            this.codigoBeneficiarioField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public st_cd_tipo_movimento codigoTipoMovimento {
        get {
            return this.codigoTipoMovimentoField;
        }
        set {
            this.codigoTipoMovimentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string tipoMovimento {
        get {
            return this.tipoMovimentoField;
        }
        set {
            this.tipoMovimentoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_erroCampoErro {
    
    private string codigoCampoField;
    
    private string descricaoCampoField;
    
    private string valorCampoField;
    
    private ct_erro_generico[] erroField;
    
    /// <remarks/>
    public string codigoCampo {
        get {
            return this.codigoCampoField;
        }
        set {
            this.codigoCampoField = value;
        }
    }
    
    /// <remarks/>
    public string descricaoCampo {
        get {
            return this.descricaoCampoField;
        }
        set {
            this.descricaoCampoField = value;
        }
    }
    
    /// <remarks/>
    public string valorCampo {
        get {
            return this.valorCampoField;
        }
        set {
            this.valorCampoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("erro")]
    public ct_erro_generico[] erro {
        get {
            return this.erroField;
        }
        set {
            this.erroField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_cd_tipo_movimento {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1")]
    Item1,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("2")]
    Item2,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("3")]
    Item3,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("4")]
    Item4,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("5")]
    Item5,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("6")]
    Item6,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_resumo_processamento {
    
    private ct_resumo_processamentoProtocoloProcessamento protocoloProcessamentoField;
    
    private ct_resumo_processamentoRegistroRejeitado[] registrosRejeitadosField;
    
    private ct_resumo_processamentoRegistroIncluido[] registrosIncluidosField;
    
    private ct_resumo_processamentoConsolidado consolidadoField;
    
    /// <remarks/>
    public ct_resumo_processamentoProtocoloProcessamento protocoloProcessamento {
        get {
            return this.protocoloProcessamentoField;
        }
        set {
            this.protocoloProcessamentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlArrayItemAttribute("registroRejeitado", IsNullable=false)]
    public ct_resumo_processamentoRegistroRejeitado[] registrosRejeitados {
        get {
            return this.registrosRejeitadosField;
        }
        set {
            this.registrosRejeitadosField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlArrayItemAttribute("registroIncluido", IsNullable=false)]
    public ct_resumo_processamentoRegistroIncluido[] registrosIncluidos {
        get {
            return this.registrosIncluidosField;
        }
        set {
            this.registrosIncluidosField = value;
        }
    }
    
    /// <remarks/>
    public ct_resumo_processamentoConsolidado consolidado {
        get {
            return this.consolidadoField;
        }
        set {
            this.consolidadoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_resumo_processamentoProtocoloProcessamento {
    
    private string numeroProtocoloField;
    
    private string nomeArquivoField;
    
    /// <remarks/>
    public string numeroProtocolo {
        get {
            return this.numeroProtocoloField;
        }
        set {
            this.numeroProtocoloField = value;
        }
    }
    
    /// <remarks/>
    public string nomeArquivo {
        get {
            return this.nomeArquivoField;
        }
        set {
            this.nomeArquivoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_resumo_processamentoRegistroRejeitado : ct_erro {
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_resumo_processamentoRegistroIncluido : ct_cco {
    
    private string codigoBeneficiarioField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string codigoBeneficiario {
        get {
            return this.codigoBeneficiarioField;
        }
        set {
            this.codigoBeneficiarioField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_resumo_processamentoConsolidado {
    
    private ct_resumo_processamentoConsolidadoConsolidadoErroProcessamento[] consolidadoErroProcessamentoField;
    
    private ct_resumo_processamentoConsolidadoConsolidadoProcessamento[] consolidadoProcessamentoField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("consolidadoErroProcessamento")]
    public ct_resumo_processamentoConsolidadoConsolidadoErroProcessamento[] consolidadoErroProcessamento {
        get {
            return this.consolidadoErroProcessamentoField;
        }
        set {
            this.consolidadoErroProcessamentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("consolidadoProcessamento")]
    public ct_resumo_processamentoConsolidadoConsolidadoProcessamento[] consolidadoProcessamento {
        get {
            return this.consolidadoProcessamentoField;
        }
        set {
            this.consolidadoProcessamentoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_resumo_processamentoConsolidadoConsolidadoErroProcessamento {
    
    private ct_erro_consolidado[] erroField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("erro")]
    public ct_erro_consolidado[] erro {
        get {
            return this.erroField;
        }
        set {
            this.erroField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_resumo_processamentoConsolidadoConsolidadoProcessamento {
    
    private ct_consolidado_processamento consolidadoInclusaoField;
    
    private ct_consolidado_processamento consolidadoRetificacaoField;
    
    private ct_consolidado_processamento consolidadoMudancaContratualField;
    
    private ct_consolidado_processamento consolidadoReativacaoField;
    
    private ct_consolidado_processamento consolidadoCancelamentoField;
    
    private ct_consolidado_processamento consolidadoSemMovimentacaoField;
    
    private ct_consolidado_processamento consolidadoTotalField;
    
    /// <remarks/>
    public ct_consolidado_processamento consolidadoInclusao {
        get {
            return this.consolidadoInclusaoField;
        }
        set {
            this.consolidadoInclusaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_consolidado_processamento consolidadoRetificacao {
        get {
            return this.consolidadoRetificacaoField;
        }
        set {
            this.consolidadoRetificacaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_consolidado_processamento consolidadoMudancaContratual {
        get {
            return this.consolidadoMudancaContratualField;
        }
        set {
            this.consolidadoMudancaContratualField = value;
        }
    }
    
    /// <remarks/>
    public ct_consolidado_processamento consolidadoReativacao {
        get {
            return this.consolidadoReativacaoField;
        }
        set {
            this.consolidadoReativacaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_consolidado_processamento consolidadoCancelamento {
        get {
            return this.consolidadoCancelamentoField;
        }
        set {
            this.consolidadoCancelamentoField = value;
        }
    }
    
    /// <remarks/>
    public ct_consolidado_processamento consolidadoSemMovimentacao {
        get {
            return this.consolidadoSemMovimentacaoField;
        }
        set {
            this.consolidadoSemMovimentacaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_consolidado_processamento consolidadoTotal {
        get {
            return this.consolidadoTotalField;
        }
        set {
            this.consolidadoTotalField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_nao_envio_beneficiarios {
    
    private st_cd_motivo_nao_envio_beneficiarios motivoNaoEnvioBeneficiariosField;
    
    /// <remarks/>
    public st_cd_motivo_nao_envio_beneficiarios motivoNaoEnvioBeneficiarios {
        get {
            return this.motivoNaoEnvioBeneficiariosField;
        }
        set {
            this.motivoNaoEnvioBeneficiariosField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_cd_motivo_nao_envio_beneficiarios {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("61")]
    Item61,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("62")]
    Item62,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_beneficiarios {
    
    private object[] itemsField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("cancelamento", typeof(ct_exclusao_beneficiario))]
    [System.Xml.Serialization.XmlElementAttribute("inclusao", typeof(ct_inclusao_beneficiario))]
    [System.Xml.Serialization.XmlElementAttribute("mudancaContratual", typeof(ct_mudanca_contratual))]
    [System.Xml.Serialization.XmlElementAttribute("reativacao", typeof(ct_reativacao))]
    [System.Xml.Serialization.XmlElementAttribute("retificacao", typeof(ct_retificacao_beneficiario))]
    public object[] Items {
        get {
            return this.itemsField;
        }
        set {
            this.itemsField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_exclusao_beneficiario {
    
    private string ccoField;
    
    private System.DateTime dataCancelamentoField;
    
    private st_cd_motivo_cancelamento motivoCancelamentoField;
    
    /// <remarks/>
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataCancelamento {
        get {
            return this.dataCancelamentoField;
        }
        set {
            this.dataCancelamentoField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_motivo_cancelamento motivoCancelamento {
        get {
            return this.motivoCancelamentoField;
        }
        set {
            this.motivoCancelamentoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_inclusao_beneficiario {
    
    private ct_identificacao_beneficiario identificacaoField;
    
    private ct_endereco_beneficiario_inclusao enderecoField;
    
    private ct_vinculo_inclusao vinculoField;
    
    /// <remarks/>
    public ct_identificacao_beneficiario identificacao {
        get {
            return this.identificacaoField;
        }
        set {
            this.identificacaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_endereco_beneficiario_inclusao endereco {
        get {
            return this.enderecoField;
        }
        set {
            this.enderecoField = value;
        }
    }
    
    /// <remarks/>
    public ct_vinculo_inclusao vinculo {
        get {
            return this.vinculoField;
        }
        set {
            this.vinculoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_identificacao_beneficiario {
    
    private string cpfField;
    
    private string dnField;
    
    private string pisPasepField;
    
    private string cnsField;
    
    private string nomeField;
    
    private st_cd_sexo sexoField;
    
    private System.DateTime dataNascimentoField;
    
    private string nomeMaeField;
    
    /// <remarks/>
    public string cpf {
        get {
            return this.cpfField;
        }
        set {
            this.cpfField = value;
        }
    }
    
    /// <remarks/>
    public string dn {
        get {
            return this.dnField;
        }
        set {
            this.dnField = value;
        }
    }
    
    /// <remarks/>
    public string pisPasep {
        get {
            return this.pisPasepField;
        }
        set {
            this.pisPasepField = value;
        }
    }
    
    /// <remarks/>
    public string cns {
        get {
            return this.cnsField;
        }
        set {
            this.cnsField = value;
        }
    }
    
    /// <remarks/>
    public string nome {
        get {
            return this.nomeField;
        }
        set {
            this.nomeField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_sexo sexo {
        get {
            return this.sexoField;
        }
        set {
            this.sexoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataNascimento {
        get {
            return this.dataNascimentoField;
        }
        set {
            this.dataNascimentoField = value;
        }
    }
    
    /// <remarks/>
    public string nomeMae {
        get {
            return this.nomeMaeField;
        }
        set {
            this.nomeMaeField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_endereco_beneficiario_inclusao {
    
    private string logradouroField;
    
    private string numeroField;
    
    private string complementoField;
    
    private string bairroField;
    
    private string codigoMunicipioField;
    
    private string codigoMunicipioResidenciaField;
    
    private string cepField;
    
    private st_tp_endereco tipoEnderecoField;
    
    private bool tipoEnderecoFieldSpecified;
    
    private st_in_reside_exterior resideExteriorField;
    
    private bool resideExteriorFieldSpecified;
    
    /// <remarks/>
    public string logradouro {
        get {
            return this.logradouroField;
        }
        set {
            this.logradouroField = value;
        }
    }
    
    /// <remarks/>
    public string numero {
        get {
            return this.numeroField;
        }
        set {
            this.numeroField = value;
        }
    }
    
    /// <remarks/>
    public string complemento {
        get {
            return this.complementoField;
        }
        set {
            this.complementoField = value;
        }
    }
    
    /// <remarks/>
    public string bairro {
        get {
            return this.bairroField;
        }
        set {
            this.bairroField = value;
        }
    }
    
    /// <remarks/>
    public string codigoMunicipio {
        get {
            return this.codigoMunicipioField;
        }
        set {
            this.codigoMunicipioField = value;
        }
    }
    
    /// <remarks/>
    public string codigoMunicipioResidencia {
        get {
            return this.codigoMunicipioResidenciaField;
        }
        set {
            this.codigoMunicipioResidenciaField = value;
        }
    }
    
    /// <remarks/>
    public string cep {
        get {
            return this.cepField;
        }
        set {
            this.cepField = value;
        }
    }
    
    /// <remarks/>
    public st_tp_endereco tipoEndereco {
        get {
            return this.tipoEnderecoField;
        }
        set {
            this.tipoEnderecoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool tipoEnderecoSpecified {
        get {
            return this.tipoEnderecoFieldSpecified;
        }
        set {
            this.tipoEnderecoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_in_reside_exterior resideExterior {
        get {
            return this.resideExteriorField;
        }
        set {
            this.resideExteriorField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool resideExteriorSpecified {
        get {
            return this.resideExteriorFieldSpecified;
        }
        set {
            this.resideExteriorFieldSpecified = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_vinculo_inclusao {
    
    private string codigoBeneficiarioField;
    
    private st_cd_tipo_relacao_dependencia relacaoDependenciaField;
    
    private string codigoBeneficiarioTitularField;
    
    private System.DateTime dataContratacaoField;
    
    private string[] itemsField;
    
    private ItemsChoiceType[] itemsElementNameField;
    
    private st_in_cobertura_parcial_temporaria coberturaParcialTemporariaField;
    
    private bool coberturaParcialTemporariaFieldSpecified;
    
    private st_in_itens_excluidos_cobertura itensExcluidosCoberturaField;
    
    private bool itensExcluidosCoberturaFieldSpecified;
    
    private string itemField;
    
    private ItemChoiceType2 itemElementNameField;
    
    /// <remarks/>
    public string codigoBeneficiario {
        get {
            return this.codigoBeneficiarioField;
        }
        set {
            this.codigoBeneficiarioField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_tipo_relacao_dependencia relacaoDependencia {
        get {
            return this.relacaoDependenciaField;
        }
        set {
            this.relacaoDependenciaField = value;
        }
    }
    
    /// <remarks/>
    public string codigoBeneficiarioTitular {
        get {
            return this.codigoBeneficiarioTitularField;
        }
        set {
            this.codigoBeneficiarioTitularField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataContratacao {
        get {
            return this.dataContratacaoField;
        }
        set {
            this.dataContratacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoANS", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoOperadora", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoPortabilidade", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
    public string[] Items {
        get {
            return this.itemsField;
        }
        set {
            this.itemsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemsChoiceType[] ItemsElementName {
        get {
            return this.itemsElementNameField;
        }
        set {
            this.itemsElementNameField = value;
        }
    }
    
    /// <remarks/>
    public st_in_cobertura_parcial_temporaria coberturaParcialTemporaria {
        get {
            return this.coberturaParcialTemporariaField;
        }
        set {
            this.coberturaParcialTemporariaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool coberturaParcialTemporariaSpecified {
        get {
            return this.coberturaParcialTemporariaFieldSpecified;
        }
        set {
            this.coberturaParcialTemporariaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_in_itens_excluidos_cobertura itensExcluidosCobertura {
        get {
            return this.itensExcluidosCoberturaField;
        }
        set {
            this.itensExcluidosCoberturaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool itensExcluidosCoberturaSpecified {
        get {
            return this.itensExcluidosCoberturaFieldSpecified;
        }
        set {
            this.itensExcluidosCoberturaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("caepfEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("ceiEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("cnpjEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
    public string Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemChoiceType2 ItemElementName {
        get {
            return this.itemElementNameField;
        }
        set {
            this.itemElementNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemsChoiceType {
    
    /// <remarks/>
    numeroPlanoANS,
    
    /// <remarks/>
    numeroPlanoOperadora,
    
    /// <remarks/>
    numeroPlanoPortabilidade,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemChoiceType2 {
    
    /// <remarks/>
    caepfEmpresaContratante,
    
    /// <remarks/>
    ceiEmpresaContratante,
    
    /// <remarks/>
    cnpjEmpresaContratante,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_mudanca_contratual {
    
    private string ccoField;
    
    private st_cd_tipo_relacao_dependencia relacaoDependenciaField;
    
    private string codigoBeneficiarioTitularField;
    
    private System.DateTime dataContratacaoField;
    
    private string[] itemsField;
    
    private ItemsChoiceType2[] itemsElementNameField;
    
    private st_in_cobertura_parcial_temporaria coberturaParcialTemporariaField;
    
    private bool coberturaParcialTemporariaFieldSpecified;
    
    private st_in_itens_excluidos_cobertura itensExcluidosCoberturaField;
    
    private bool itensExcluidosCoberturaFieldSpecified;
    
    private string itemField;
    
    private ItemChoiceType4 itemElementNameField;
    
    /// <remarks/>
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_tipo_relacao_dependencia relacaoDependencia {
        get {
            return this.relacaoDependenciaField;
        }
        set {
            this.relacaoDependenciaField = value;
        }
    }
    
    /// <remarks/>
    public string codigoBeneficiarioTitular {
        get {
            return this.codigoBeneficiarioTitularField;
        }
        set {
            this.codigoBeneficiarioTitularField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataContratacao {
        get {
            return this.dataContratacaoField;
        }
        set {
            this.dataContratacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoANS", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoOperadora", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoPortabilidade", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
    public string[] Items {
        get {
            return this.itemsField;
        }
        set {
            this.itemsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemsChoiceType2[] ItemsElementName {
        get {
            return this.itemsElementNameField;
        }
        set {
            this.itemsElementNameField = value;
        }
    }
    
    /// <remarks/>
    public st_in_cobertura_parcial_temporaria coberturaParcialTemporaria {
        get {
            return this.coberturaParcialTemporariaField;
        }
        set {
            this.coberturaParcialTemporariaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool coberturaParcialTemporariaSpecified {
        get {
            return this.coberturaParcialTemporariaFieldSpecified;
        }
        set {
            this.coberturaParcialTemporariaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_in_itens_excluidos_cobertura itensExcluidosCobertura {
        get {
            return this.itensExcluidosCoberturaField;
        }
        set {
            this.itensExcluidosCoberturaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool itensExcluidosCoberturaSpecified {
        get {
            return this.itensExcluidosCoberturaFieldSpecified;
        }
        set {
            this.itensExcluidosCoberturaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("caepfEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("ceiEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("cnpjEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
    public string Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemChoiceType4 ItemElementName {
        get {
            return this.itemElementNameField;
        }
        set {
            this.itemElementNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemsChoiceType2 {
    
    /// <remarks/>
    numeroPlanoANS,
    
    /// <remarks/>
    numeroPlanoOperadora,
    
    /// <remarks/>
    numeroPlanoPortabilidade,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemChoiceType4 {
    
    /// <remarks/>
    caepfEmpresaContratante,
    
    /// <remarks/>
    ceiEmpresaContratante,
    
    /// <remarks/>
    cnpjEmpresaContratante,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_reativacao {
    
    private string ccoField;
    
    private System.DateTime dataReativacaoField;
    
    /// <remarks/>
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataReativacao {
        get {
            return this.dataReativacaoField;
        }
        set {
            this.dataReativacaoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_retificacao_beneficiario {
    
    private string ccoField;
    
    private ct_identificacao_beneficiario_retificacao identificacaoField;
    
    private ct_endereco_beneficiario_retificacao enderecoField;
    
    private ct_vinculo_retificacao vinculoField;
    
    private string codigoProcedimentoAdministrativoField;
    
    /// <remarks/>
    public string cco {
        get {
            return this.ccoField;
        }
        set {
            this.ccoField = value;
        }
    }
    
    /// <remarks/>
    public ct_identificacao_beneficiario_retificacao identificacao {
        get {
            return this.identificacaoField;
        }
        set {
            this.identificacaoField = value;
        }
    }
    
    /// <remarks/>
    public ct_endereco_beneficiario_retificacao endereco {
        get {
            return this.enderecoField;
        }
        set {
            this.enderecoField = value;
        }
    }
    
    /// <remarks/>
    public ct_vinculo_retificacao vinculo {
        get {
            return this.vinculoField;
        }
        set {
            this.vinculoField = value;
        }
    }
    
    /// <remarks/>
    public string codigoProcedimentoAdministrativo {
        get {
            return this.codigoProcedimentoAdministrativoField;
        }
        set {
            this.codigoProcedimentoAdministrativoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_identificacao_beneficiario_retificacao {
    
    private string cpfField;
    
    private string dnField;
    
    private string pisPasepField;
    
    private string cnsField;
    
    private string nomeField;
    
    private st_cd_sexo sexoField;
    
    private bool sexoFieldSpecified;
    
    private System.DateTime dataNascimentoField;
    
    private bool dataNascimentoFieldSpecified;
    
    private string nomeMaeField;
    
    /// <remarks/>
    public string cpf {
        get {
            return this.cpfField;
        }
        set {
            this.cpfField = value;
        }
    }
    
    /// <remarks/>
    public string dn {
        get {
            return this.dnField;
        }
        set {
            this.dnField = value;
        }
    }
    
    /// <remarks/>
    public string pisPasep {
        get {
            return this.pisPasepField;
        }
        set {
            this.pisPasepField = value;
        }
    }
    
    /// <remarks/>
    public string cns {
        get {
            return this.cnsField;
        }
        set {
            this.cnsField = value;
        }
    }
    
    /// <remarks/>
    public string nome {
        get {
            return this.nomeField;
        }
        set {
            this.nomeField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_sexo sexo {
        get {
            return this.sexoField;
        }
        set {
            this.sexoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool sexoSpecified {
        get {
            return this.sexoFieldSpecified;
        }
        set {
            this.sexoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataNascimento {
        get {
            return this.dataNascimentoField;
        }
        set {
            this.dataNascimentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool dataNascimentoSpecified {
        get {
            return this.dataNascimentoFieldSpecified;
        }
        set {
            this.dataNascimentoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public string nomeMae {
        get {
            return this.nomeMaeField;
        }
        set {
            this.nomeMaeField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_endereco_beneficiario_retificacao {
    
    private string logradouroField;
    
    private string numeroField;
    
    private string complementoField;
    
    private string bairroField;
    
    private string codigoMunicipioField;
    
    private string codigoMunicipioResidenciaField;
    
    private string cepField;
    
    private st_tp_endereco tipoEnderecoField;
    
    private bool tipoEnderecoFieldSpecified;
    
    private st_in_reside_exterior resideExteriorField;
    
    private bool resideExteriorFieldSpecified;
    
    /// <remarks/>
    public string logradouro {
        get {
            return this.logradouroField;
        }
        set {
            this.logradouroField = value;
        }
    }
    
    /// <remarks/>
    public string numero {
        get {
            return this.numeroField;
        }
        set {
            this.numeroField = value;
        }
    }
    
    /// <remarks/>
    public string complemento {
        get {
            return this.complementoField;
        }
        set {
            this.complementoField = value;
        }
    }
    
    /// <remarks/>
    public string bairro {
        get {
            return this.bairroField;
        }
        set {
            this.bairroField = value;
        }
    }
    
    /// <remarks/>
    public string codigoMunicipio {
        get {
            return this.codigoMunicipioField;
        }
        set {
            this.codigoMunicipioField = value;
        }
    }
    
    /// <remarks/>
    public string codigoMunicipioResidencia {
        get {
            return this.codigoMunicipioResidenciaField;
        }
        set {
            this.codigoMunicipioResidenciaField = value;
        }
    }
    
    /// <remarks/>
    public string cep {
        get {
            return this.cepField;
        }
        set {
            this.cepField = value;
        }
    }
    
    /// <remarks/>
    public st_tp_endereco tipoEndereco {
        get {
            return this.tipoEnderecoField;
        }
        set {
            this.tipoEnderecoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool tipoEnderecoSpecified {
        get {
            return this.tipoEnderecoFieldSpecified;
        }
        set {
            this.tipoEnderecoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_in_reside_exterior resideExterior {
        get {
            return this.resideExteriorField;
        }
        set {
            this.resideExteriorField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool resideExteriorSpecified {
        get {
            return this.resideExteriorFieldSpecified;
        }
        set {
            this.resideExteriorFieldSpecified = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_vinculo_retificacao {
    
    private string codigoBeneficiarioField;
    
    private st_cd_tipo_relacao_dependencia relacaoDependenciaField;
    
    private bool relacaoDependenciaFieldSpecified;
    
    private string codigoBeneficiarioTitularField;
    
    private System.DateTime dataContratacaoField;
    
    private bool dataContratacaoFieldSpecified;
    
    private System.DateTime dataReativacaoField;
    
    private bool dataReativacaoFieldSpecified;
    
    private System.DateTime dataCancelamentoField;
    
    private bool dataCancelamentoFieldSpecified;
    
    private st_cd_motivo_cancelamento motivoCancelamentoField;
    
    private bool motivoCancelamentoFieldSpecified;
    
    private string[] itemsField;
    
    private ItemsChoiceType1[] itemsElementNameField;
    
    private st_in_cobertura_parcial_temporaria coberturaParcialTemporariaField;
    
    private bool coberturaParcialTemporariaFieldSpecified;
    
    private st_in_itens_excluidos_cobertura itensExcluidosCoberturaField;
    
    private bool itensExcluidosCoberturaFieldSpecified;
    
    private string itemField;
    
    private ItemChoiceType3 itemElementNameField;
    
    /// <remarks/>
    public string codigoBeneficiario {
        get {
            return this.codigoBeneficiarioField;
        }
        set {
            this.codigoBeneficiarioField = value;
        }
    }
    
    /// <remarks/>
    public st_cd_tipo_relacao_dependencia relacaoDependencia {
        get {
            return this.relacaoDependenciaField;
        }
        set {
            this.relacaoDependenciaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool relacaoDependenciaSpecified {
        get {
            return this.relacaoDependenciaFieldSpecified;
        }
        set {
            this.relacaoDependenciaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public string codigoBeneficiarioTitular {
        get {
            return this.codigoBeneficiarioTitularField;
        }
        set {
            this.codigoBeneficiarioTitularField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataContratacao {
        get {
            return this.dataContratacaoField;
        }
        set {
            this.dataContratacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool dataContratacaoSpecified {
        get {
            return this.dataContratacaoFieldSpecified;
        }
        set {
            this.dataContratacaoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataReativacao {
        get {
            return this.dataReativacaoField;
        }
        set {
            this.dataReativacaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool dataReativacaoSpecified {
        get {
            return this.dataReativacaoFieldSpecified;
        }
        set {
            this.dataReativacaoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime dataCancelamento {
        get {
            return this.dataCancelamentoField;
        }
        set {
            this.dataCancelamentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool dataCancelamentoSpecified {
        get {
            return this.dataCancelamentoFieldSpecified;
        }
        set {
            this.dataCancelamentoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_cd_motivo_cancelamento motivoCancelamento {
        get {
            return this.motivoCancelamentoField;
        }
        set {
            this.motivoCancelamentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool motivoCancelamentoSpecified {
        get {
            return this.motivoCancelamentoFieldSpecified;
        }
        set {
            this.motivoCancelamentoFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoANS", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoOperadora", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("numeroPlanoPortabilidade", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
    public string[] Items {
        get {
            return this.itemsField;
        }
        set {
            this.itemsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemsChoiceType1[] ItemsElementName {
        get {
            return this.itemsElementNameField;
        }
        set {
            this.itemsElementNameField = value;
        }
    }
    
    /// <remarks/>
    public st_in_cobertura_parcial_temporaria coberturaParcialTemporaria {
        get {
            return this.coberturaParcialTemporariaField;
        }
        set {
            this.coberturaParcialTemporariaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool coberturaParcialTemporariaSpecified {
        get {
            return this.coberturaParcialTemporariaFieldSpecified;
        }
        set {
            this.coberturaParcialTemporariaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public st_in_itens_excluidos_cobertura itensExcluidosCobertura {
        get {
            return this.itensExcluidosCoberturaField;
        }
        set {
            this.itensExcluidosCoberturaField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool itensExcluidosCoberturaSpecified {
        get {
            return this.itensExcluidosCoberturaFieldSpecified;
        }
        set {
            this.itensExcluidosCoberturaFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("caepfEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("ceiEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("cnpjEmpresaContratante", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
    public string Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemChoiceType3 ItemElementName {
        get {
            return this.itemElementNameField;
        }
        set {
            this.itemElementNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemsChoiceType1 {
    
    /// <remarks/>
    numeroPlanoANS,
    
    /// <remarks/>
    numeroPlanoOperadora,
    
    /// <remarks/>
    numeroPlanoPortabilidade,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemChoiceType3 {
    
    /// <remarks/>
    caepfEmpresaContratante,
    
    /// <remarks/>
    ceiEmpresaContratante,
    
    /// <remarks/>
    cnpjEmpresaContratante,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public  class ct_identificacao_software_gerador {
    
    private string nomeAplicativoField;
    
    private string versaoAplicativoField;
    
    private string fabricanteAplicativoField;
    
    /// <remarks/>
    public string nomeAplicativo {
        get {
            return this.nomeAplicativoField;
        }
        set {
            this.nomeAplicativoField = value;
        }
    }
    
    /// <remarks/>
    public string versaoAplicativo {
        get {
            return this.versaoAplicativoField;
        }
        set {
            this.versaoAplicativoField = value;
        }
    }
    
    /// <remarks/>
    public string fabricanteAplicativo {
        get {
            return this.fabricanteAplicativoField;
        }
        set {
            this.fabricanteAplicativoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_falha_negocioFalha {
    
    private string codigoFalhaField;
    
    private string descricaoFalhaField;
    
    /// <remarks/>
    public string codigoFalha {
        get {
            return this.codigoFalhaField;
        }
        set {
            this.codigoFalhaField = value;
        }
    }
    
    /// <remarks/>
    public string descricaoFalha {
        get {
            return this.descricaoFalhaField;
        }
        set {
            this.descricaoFalhaField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_cabecalho_transacaoOrigem {
    
    private string itemField;
    
    private ItemChoiceType itemElementNameField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("cnpj", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("registroANS", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
    public string Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemChoiceType ItemElementName {
        get {
            return this.itemElementNameField;
        }
        set {
            this.itemElementNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemChoiceType {
    
    /// <remarks/>
    cnpj,
    
    /// <remarks/>
    registroANS,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class ct_cabecalho_transacaoDestino {
    
    private string itemField;
    
    private ItemChoiceType1 itemElementNameField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("cnpj", typeof(string))]
    [System.Xml.Serialization.XmlElementAttribute("registroANS", typeof(string))]
    [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
    public string Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public ItemChoiceType1 ItemElementName {
        get {
            return this.itemElementNameField;
        }
        set {
            this.itemElementNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema=false)]
public enum ItemChoiceType1 {
    
    /// <remarks/>
    cnpj,
    
    /// <remarks/>
    registroANS,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_nr_versao {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("1.1")]
    Item11,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class mensagemSIBMensagem {
    
    private object itemField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("ansParaOperadora", typeof(mensagemSIBMensagemAnsParaOperadora), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Xml.Serialization.XmlElementAttribute("operadoraParaANS", typeof(mensagemSIBMensagemOperadoraParaANS), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class mensagemSIBMensagemAnsParaOperadora {
    
    private object itemField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("conferencia", typeof(ct_conferencia), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Xml.Serialization.XmlElementAttribute("resultadoProcessamento", typeof(mensagemSIBMensagemAnsParaOperadoraResultadoProcessamento), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Xml.Serialization.XmlElementAttribute("transferencia", typeof(mensagemSIBMensagemAnsParaOperadoraTransferencia), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class mensagemSIBMensagemAnsParaOperadoraResultadoProcessamento {
    
    private object itemField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("arquivoProcessado", typeof(ct_resumo_processamento), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Xml.Serialization.XmlElementAttribute("arquivoRejeitado", typeof(mensagemSIBMensagemAnsParaOperadoraResultadoProcessamentoArquivoRejeitado), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class mensagemSIBMensagemAnsParaOperadoraResultadoProcessamentoArquivoRejeitado {
    
    private string nomeArquivoField;
    
    private double tamanhoArquivoField;
    
    private System.DateTime dataHoraRecebimentoField;
    
    private string enderecoIPOrigemField;
    
    private st_cd_erro_rejeicao_arquivo codigoMotivoRejeicaoField;
    
    private string motivoRejeicaoField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string nomeArquivo {
        get {
            return this.nomeArquivoField;
        }
        set {
            this.nomeArquivoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public double tamanhoArquivo {
        get {
            return this.tamanhoArquivoField;
        }
        set {
            this.tamanhoArquivoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public System.DateTime dataHoraRecebimento {
        get {
            return this.dataHoraRecebimentoField;
        }
        set {
            this.dataHoraRecebimentoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string enderecoIPOrigem {
        get {
            return this.enderecoIPOrigemField;
        }
        set {
            this.enderecoIPOrigemField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public st_cd_erro_rejeicao_arquivo codigoMotivoRejeicao {
        get {
            return this.codigoMotivoRejeicaoField;
        }
        set {
            this.codigoMotivoRejeicaoField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string motivoRejeicao {
        get {
            return this.motivoRejeicaoField;
        }
        set {
            this.motivoRejeicaoField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
public enum st_cd_erro_rejeicao_arquivo {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("16")]
    Item16,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("17")]
    Item17,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("18")]
    Item18,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("19")]
    Item19,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("22")]
    Item22,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("23")]
    Item23,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("24")]
    Item24,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("26")]
    Item26,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("20")]
    Item20,
    
    /// <remarks/>
    [System.Xml.Serialization.XmlEnumAttribute("21")]
    Item21,
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class mensagemSIBMensagemAnsParaOperadoraTransferencia {
    
    private ct_tab tabField;
    
    private ct_conferencia_transferencia itemField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ct_tab tab {
        get {
            return this.tabField;
        }
        set {
            this.tabField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("conferencia", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ct_conferencia_transferencia Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public  class mensagemSIBMensagemOperadoraParaANS {
    
    private object itemField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("beneficiarios", typeof(ct_beneficiarios), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Xml.Serialization.XmlElementAttribute("naoEnvioBeneficiarios", typeof(ct_nao_envio_beneficiarios), Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public object Item {
        get {
            return this.itemField;
        }
        set {
            this.itemField = value;
        }
    }
}

