﻿using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using static XMLJean.Objetos.ObjBeneficiario;

const string connectionString = "Data Source=localhost;Initial Catalog=TesteJeanXML;User Id=sa;Password=6Dimmuborgir6";

Console.WriteLine($"Inicio: {DateTime.Now:dd/MM/yyyy HH:mm:ss}");

var nomeArquivo = String.Concat(Environment.CurrentDirectory, @"\XML\ArqConf3048831120230101.XML");
var con = new SqlConnection(connectionString);
var tbl = new DataTable();

await TruncateTable();
MontagemTabelaMemoria();

var xmlDoc = XDocument.Parse(File.ReadAllText(nomeArquivo));
var beneficiarios = xmlDoc.Descendants("beneficiario");

foreach (XElement beneficiario in beneficiarios)
{
    var stream = new MemoryStream(Encoding.UTF8.GetBytes(beneficiario.ToString()));
    var serializer = new XmlSerializer(typeof(Beneficiario));
    var dados = serializer.Deserialize(stream) as Beneficiario;
    EscrevendoDadosTabelaMemoria(dados);
}

await ExecutandoBulkInsert();

Console.WriteLine($" Final: {DateTime.Now:dd/MM/yyyy HH:mm:ss}");

#region METODOS PRIVADOS AUXILIARES
async Task TruncateTable()
{
    SqlCommand com = new("TRUNCATE TABLE tblJeanXML")
    {
        CommandType = CommandType.Text,
        Connection = con
    };

    await OpenConnection();
    await com.ExecuteNonQueryAsync();
    await CloseConnection();
}

async Task ExecutandoBulkInsert()
{
    SqlBulkCopy objbulk = new(con)
    {
        DestinationTableName = "tblJeanXML"
    };

    objbulk.ColumnMappings.Add("cco", "cco");
    objbulk.ColumnMappings.Add("situacao", "situacao");
    objbulk.ColumnMappings.Add("codigoBeneficiario", "codigoBeneficiario");
    objbulk.ColumnMappings.Add("ccoBeneficiarioTitular", "ccoBeneficiarioTitular");
    objbulk.ColumnMappings.Add("dataAtualizacao", "dataAtualizacao");
    objbulk.ColumnMappings.Add("cpf", "cpf");
    objbulk.ColumnMappings.Add("pisPasep", "pisPasep");
    objbulk.ColumnMappings.Add("CNS", "CNS");
    objbulk.ColumnMappings.Add("nome", "nome");
    objbulk.ColumnMappings.Add("sexo", "sexo");
    objbulk.ColumnMappings.Add("dataNascimento", "dataNascimento");
    objbulk.ColumnMappings.Add("nomeMae", "nomeMae");
    objbulk.ColumnMappings.Add("tipoEndereco", "tipoEndereco");
    objbulk.ColumnMappings.Add("logradouro", "logradouro");
    objbulk.ColumnMappings.Add("numero", "numero");
    objbulk.ColumnMappings.Add("bairro", "bairro");
    objbulk.ColumnMappings.Add("codigoMunicipio", "codigoMunicipio");
    objbulk.ColumnMappings.Add("cep", "cep");
    objbulk.ColumnMappings.Add("resideExterior", "resideExterior");
    objbulk.ColumnMappings.Add("relacaoDependencia", "relacaoDependencia");
    objbulk.ColumnMappings.Add("dataContratacao", "dataContratacao");
    objbulk.ColumnMappings.Add("dataReativacao", "dataReativacao");
    objbulk.ColumnMappings.Add("dataCancelamento", "dataCancelamento");
    objbulk.ColumnMappings.Add("motivoCancelamento", "motivoCancelamento");
    objbulk.ColumnMappings.Add("numeroPlanoANS", "numeroPlanoANS");
    objbulk.ColumnMappings.Add("numeroPlanoOperadora", "numeroPlanoOperadora");
    objbulk.ColumnMappings.Add("coberturaParcialTemporaria", "coberturaParcialTemporaria");
    objbulk.ColumnMappings.Add("itensExcluidosCobertura", "itensExcluidosCobertura");
    objbulk.ColumnMappings.Add("cnpjEmpresaContratante", "cnpjEmpresaContratante");
    objbulk.ColumnMappings.Add("dataPrimeiraContratacao", "dataPrimeiraContratacao");

    await OpenConnection();
    await objbulk.WriteToServerAsync(tbl);
    await CloseConnection();
}

void EscrevendoDadosTabelaMemoria(Beneficiario dados)
{
    DataRow dr = tbl.NewRow();
    dr["cco"] = dados.Cco;
    dr["situacao"] = dados.Situacao;
    dr["codigoBeneficiario"] = dados.Vinculo?.CodigoBeneficiario;
    dr["ccoBeneficiarioTitular"] = dados.Vinculo?.CcoBeneficiarioTitular;
    dr["dataAtualizacao"] = dados.DataAtualizacao;
    dr["cpf"] = dados.Identificacao?.Cpf;
    dr["pisPasep"] = string.Empty;
    dr["CNS"] = dados.Identificacao?.Cns;
    dr["nome"] = dados.Identificacao?.Nome;
    dr["sexo"] = dados.Identificacao?.Sexo;
    dr["dataNascimento"] = dados.Identificacao?.DataNascimento;
    dr["nomeMae"] = dados.Identificacao?.NomeMae;
    dr["tipoEndereco"] = dados.Endereco?.TipoEndereco;
    dr["logradouro"] = dados.Endereco?.Logradouro;
    dr["numero"] = dados.Endereco?.Numero;
    dr["bairro"] = dados.Endereco?.Bairro;
    dr["codigoMunicipio"] = dados.Endereco?.CodigoMunicipio;
    dr["cep"] = dados.Endereco?.Cep;
    dr["resideExterior"] = dados.Endereco?.ResideExterior;
    dr["relacaoDependencia"] = dados.Vinculo?.RelacaoDependencia;
    dr["dataContratacao"] = dados.Vinculo?.DataContratacao;
    dr["dataReativacao"] = string.Empty;
    dr["dataCancelamento"] = dados.Vinculo?.DataCancelamento;
    dr["motivoCancelamento"] = dados.Vinculo?.MotivoCancelamento;
    dr["numeroPlanoANS"] = dados.Vinculo?.NumeroPlanoANS;
    dr["numeroPlanoOperadora"] = string.Empty;
    dr["coberturaParcialTemporaria"] = dados.Vinculo?.CoberturaParcialTemporaria;
    dr["itensExcluidosCobertura"] = dados.Vinculo?.ItensExcluidosCobertura;
    dr["cnpjEmpresaContratante"] = dados.Vinculo?.CnpjEmpresaContratante;
    dr["dataPrimeiraContratacao"] = dados.Vinculo?.DataPrimeiraContratacao;
    tbl.Rows.Add(dr);
}

void MontagemTabelaMemoria()
{
    tbl.Columns.Add(new DataColumn("cco", typeof(string)));
    tbl.Columns.Add(new DataColumn("situacao", typeof(string)));
    tbl.Columns.Add(new DataColumn("codigoBeneficiario", typeof(string)));
    tbl.Columns.Add(new DataColumn("ccoBeneficiarioTitular", typeof(string)));
    tbl.Columns.Add(new DataColumn("dataAtualizacao", typeof(string)));
    tbl.Columns.Add(new DataColumn("cpf", typeof(string)));
    tbl.Columns.Add(new DataColumn("pisPasep", typeof(string)));
    tbl.Columns.Add(new DataColumn("CNS", typeof(string)));
    tbl.Columns.Add(new DataColumn("nome", typeof(string)));
    tbl.Columns.Add(new DataColumn("sexo", typeof(string)));
    tbl.Columns.Add(new DataColumn("dataNascimento", typeof(string)));
    tbl.Columns.Add(new DataColumn("nomeMae", typeof(string)));
    tbl.Columns.Add(new DataColumn("tipoEndereco", typeof(string)));
    tbl.Columns.Add(new DataColumn("logradouro", typeof(string)));
    tbl.Columns.Add(new DataColumn("numero", typeof(string)));
    tbl.Columns.Add(new DataColumn("bairro", typeof(string)));
    tbl.Columns.Add(new DataColumn("codigoMunicipio", typeof(string)));
    tbl.Columns.Add(new DataColumn("cep", typeof(string)));
    tbl.Columns.Add(new DataColumn("resideExterior", typeof(string)));
    tbl.Columns.Add(new DataColumn("relacaoDependencia", typeof(string)));
    tbl.Columns.Add(new DataColumn("dataContratacao", typeof(string)));
    tbl.Columns.Add(new DataColumn("dataReativacao", typeof(string)));
    tbl.Columns.Add(new DataColumn("dataCancelamento", typeof(string)));
    tbl.Columns.Add(new DataColumn("motivoCancelamento", typeof(string)));
    tbl.Columns.Add(new DataColumn("numeroPlanoANS", typeof(string)));
    tbl.Columns.Add(new DataColumn("numeroPlanoOperadora", typeof(string)));
    tbl.Columns.Add(new DataColumn("coberturaParcialTemporaria", typeof(string)));
    tbl.Columns.Add(new DataColumn("itensExcluidosCobertura", typeof(string)));
    tbl.Columns.Add(new DataColumn("cnpjEmpresaContratante", typeof(string)));
    tbl.Columns.Add(new DataColumn("dataPrimeiraContratacao", typeof(string)));
}

async Task OpenConnection()
{
    await con.OpenAsync();
}

async Task CloseConnection()
{
    await con.CloseAsync();
}
#endregion

//=================================================================================================================
//Exemplo descendo via objeto geral (descontinuado é mais trabalhoso)
//=================================================================================================================
//MemoryStream stream = new(Encoding.UTF8.GetBytes(System.IO.File.ReadAllText(nomeArquivo)));
//var s = new System.Xml.Serialization.XmlSerializer(typeof(mensagemSIB));
//mensagemSIB objetoComplexo = (mensagemSIB)s.Deserialize(XmlReader.Create(stream));